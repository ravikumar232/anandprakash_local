<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */



class Amasty_Abtesting_Block_Adminhtml_Form_Element_Status extends Varien_Data_Form_Element_Text
{
    public function getElementHtml()
    {
        $html = '';
        $hlp = Mage::helper('amabtesting');

        $statuses = array(
            Amasty_Abtesting_Model_Test::STATUS_SETUP   => array(
                'title' => '▶ Run Experiment',
                'color' => '#AAAAAA',
            ),
            Amasty_Abtesting_Model_Test::STATUS_PAUSED  => array(
                'title' => '▶ Resume Experiment',
                'color' => '#E60707',
            ),
            Amasty_Abtesting_Model_Test::STATUS_RUNNING => array(
                'title' => '▮▮ Pause Experiment',
                'color' => '#41AB0F',
            ),
        );


        $value = $this->getValue();

        if ($values = $this->getValues()) {
            if (isset($values[$value])) {
                $style = '';

                if (isset($statuses[$value]['color'])) {
                    $style = " style='color:{$statuses[$value]['color']}'";
                }

                $html = "<strong$style>$values[$value]</strong>";
            }
        }


        if (isset($statuses[$value])) {
            $block = Mage::app()->getLayout()->createBlock(
                'adminhtml/widget_button'
            );

            $block->setData(
                array(
                    'label' => $hlp->__($statuses[$value]['title']),
                    'style' => 'float:right;margin-right: 20px;',
                    'type'  => 'submit',
                    'element_name'  => 'update_status',
                    'value' => '1'
                )
            );

            // Prevent button pressing on pressing "Enter" key in any form field
            $html .= '<button style="display: none;" /> ' . $block->toHtml();
        }

        return $html;
    }
}
