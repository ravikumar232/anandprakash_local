<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Block_Adminhtml_Test_New_Tab_Attributes extends Mage_Adminhtml_Block_Template
{
    protected function _construct()
    {
        $this->setTemplate('amasty/amabtesting/attributes.phtml');

        parent::_construct();
    }

    public function getAttributes()
    {
        $collection = Mage::getResourceModel('catalog/product_attribute_collection')
            ->addFieldToFilter(
                array(
                    'visible' => 'additional_table.is_visible',
                    'code' => 'main_table.attribute_code'
                ),
                array(
                    'visible' => 1,
                    'code'    => 'amabtesting_css_override'
                )
            )
            ->addFieldToFilter(
                'main_table.frontend_input',
                array('in' => array('text', 'select', 'multiselect', 'boolean', 'textarea', 'weight', 'date'))
            )
            ->addFieldToFilter('attribute_code', array('nin' => array(
                'sku'
            )))
            ->setOrder('main_table.frontend_label', "ASC");

        return $collection;
    }
}
