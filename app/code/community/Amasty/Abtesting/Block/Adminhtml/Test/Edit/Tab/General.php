<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Block_Adminhtml_Test_Edit_Tab_General extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $model = Mage::registry('current_test');

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getData('action'),
            'method' => 'post'
        ));

        $fieldset = $form->addFieldset('common_fieldset',
            array('legend' => $this->__('Main Info'))
        );

        $fieldset->addType(
            'amasty_abtesting_status',
            'Amasty_Abtesting_Block_Adminhtml_Form_Element_Status'
        );

        $fieldset->addField('name', 'text', array(
            'name'  => 'name',
            'label' => $this->__('Experiment Name'),
            'title' => $this->__('Experiment Name'),
            'required' => true,
        ));

        $fieldset->addField('status', 'amasty_abtesting_status', array(
            'name'  => 'status',
            'label' => $this->__('Status'),
            'title' => $this->__('Status'),
            'values'=> $model->getStatuses()
        ));

        $goalField = $fieldset->addField('goal', 'select', array(
            'name'  => 'goal',
            'label' => $this->__('Goal'),
            'title' => $this->__('Goal'),
            'values'=> $model->getGoals()
        ));

        $visitUrlField = $fieldset->addField('visit_url', 'text', array(
            'name'  => 'visit_url',
            'required' => true,
            'note' => $this->__('Related to current store'),
            'value_class' => 'value visit_url',
            'label' => $this->__('Page Visited'),
            'title' => $this->__('Page Visited'),
        ));

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        }

        $autocompletion = $form->addFieldset('autocompletion_fieldset',
            array('legend' => $this->__('Auto Completion'))
        );

        $autocomplete = $autocompletion->addField('autocomplete', 'select', array(
            'name'  => 'autocomplete',
            'label' => $this->__('Auto Complete Test'),
            'title' => $this->__('Auto Complete Test'),
            'values'=> Mage::getSingleton('adminhtml/system_config_source_yesno')->toArray()
        ));

        $minDaysField = $autocompletion->addField('detect_rate', 'text', array(
            'name'  => 'detect_rate',
            'label' => $this->__('Minimum relative improvement in conversion rate you want to detect (%)'),
            'title' => $this->__('Minimum relative improvement in conversion rate you want to detect (%)'),
            'class' => 'validate-number validate-number-range number-range-1-100'
        ));

        if (!$model->getDetectRate()) {
            $model->setDetectRate(Amasty_Abtesting_Model_Test::DEFAULT_DETECT_RATE);
        }

        $form->setValues($model->getData());

        $this->setForm($form);

        $this
            ->_addDepend(
                $visitUrlField,
                $goalField,
                Amasty_Abtesting_Model_Test::GOAL_VISIT
            )
            ->_addDepend($minDaysField, $autocomplete, 1)
        ;

        return parent::_prepareForm();
    }

    protected function _addDepend($what, $from, $value)
    {
        $formAfter = $this->getChild('form_after');

        if (!$formAfter) {
            $formAfter = $this->getLayout()->createBlock('adminhtml/text_list');
            $this->setChild('form_after', $formAfter);
        }

        $formAfter->append(
            $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
                ->addFieldMap($what->getHtmlId(), $what->getName())
                ->addFieldMap($from->getHtmlId(), $from->getName())
                ->addFieldDependence(
                    $what->getName(),
                    $from->getName(),
                    $value
                )
        );

        return $this;
    }
}
