<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Block_Adminhtml_Test_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('testGrid');
        $this->setDefaultSort('id');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('amabtesting/test_collection_grid');

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'name',
            array(
                'header' => $this->__('Test Name'),
                'index'  => 'name',
                'filter_index' => 'main_table.name'
            )
        );

        $this->addColumn(
            'created_at',
            array(
                'header' => $this->__('Date Created'),
                'index'  => 'created_at',
                'type'   => 'date'
            )
        );

        $this->addColumn(
            'status',
            array(
                'header'  => $this->__('Status'),
                'index'   => 'status',
                'type'    => 'options',
                'options' => Mage::getSingleton('amabtesting/test')->getStatuses(),
            )
        );


        $this->addColumn(
            'total_visits',
            array(
                'header'  => $this->__('Total Experiment Visits'),
                'index'   => 'total_visits',
                'filter'  => false,
            )
        );

        $this->addColumn(
            'total_conversions',
            array(
                'header'  => $this->__('Total Conversions'),
                'index'   => 'total_conversions',
                'filter'  => false,
            )
        );

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}
