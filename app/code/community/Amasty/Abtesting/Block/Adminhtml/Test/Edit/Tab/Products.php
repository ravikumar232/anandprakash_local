<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Block_Adminhtml_Test_Edit_Tab_Products extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('product_grid');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $test = Mage::registry('current_test');

        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('*')
        ;

        $collection->getSelect()
            ->join(
                array('tp' => $collection->getResource()->getTable('amabtesting/test_product')),
                'tp.product_id = e.entity_id',
                array()
            )
            ->where('tp.test_id=?', $test->getId())
        ;

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header'    => $this->__('ID'),
            'sortable'  => true,
            'width'     => 60,
            'index'     => 'entity_id'
        ));

        $this->addColumn('product_name', array(
            'header'    => $this->__('Name'),
            'index'     => 'name'
        ));

        $this->addColumn('type', array(
            'header'    => $this->__('Type'),
            'width'     => 100,
            'index'     => 'type_id',
            'type'      => 'options',
            'options'   => Mage::getSingleton('catalog/product_type')->getOptionArray(),
        ));

        $this->addColumn('sku', array(
            'header'    => $this->__('SKU'),
            'width'     => 80,
            'index'     => 'sku'
        ));

        $this->addColumn('price', array(
            'header'        => $this->__('Price'),
            'type'          => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index'         => 'price'
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getData('grid_url')
            ? $this->getData('grid_url')
            : $this->getUrl('adminhtml/amabtesting/applicableProductGrid', array(
                '_current' => true,
                'test' => Mage::registry('current_test')->getId()
            ));
    }

    protected function _toHtml()
    {
        $html = parent::_toHtml();

        $html .= "<script type='text/javascript'>
$('{$this->getId()}').select('input').each(function(input){
input.observe('keypress', function(e){if (e.keyCode == Event.KEY_RETURN) {e.stop();}});});
</script>";

        return $html;
    }
}
