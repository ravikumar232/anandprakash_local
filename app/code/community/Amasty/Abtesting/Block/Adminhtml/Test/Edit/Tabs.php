<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Block_Adminhtml_Test_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('testTabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->__('Test Information'));
    }

    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
    }

    protected function _beforeToHtml()
    {
        $test = Mage::registry('current_test');
        if ($test->getStatus() != Amasty_Abtesting_Model_Test::STATUS_SETUP) {
            $this->addTab('report', array(
                'label'     => $this->__('Report'),
                'title'     => $this->__('Report'),
                'content'   => $this->getLayout()
                    ->createBlock('amabtesting/adminhtml_test_edit_tab_report')->toHtml(),
            ));
        }

        $this->addTab('general', array(
            'label'     => $this->__('General'),
            'title'     => $this->__('General'),
            'content'   => $this->getLayout()
                ->createBlock('amabtesting/adminhtml_test_edit_tab_general')->toHtml(),
        ));

        $this->addTab('products', array(
            'label'     => $this->__('Applicable Products'),
            'title'     => $this->__('Applicable Products'),
            'content'   => $this->getLayout()
                ->createBlock('amabtesting/adminhtml_test_edit_tab_products')->toHtml(),
        ));

        $variations = $test->getVariations();

        $counter = 0;
        foreach ($variations as $variation) {
            $counter++;

            $tab = $this->getLayout()
                ->createBlock('amabtesting/adminhtml_test_edit_tab_variation')
                ->setVariation($variation)
                ->setIndex($counter)
            ;

            $this->addTab('variation_'.$counter, array(
                'label'     => $this->__('Variation %s', $counter),
                'title'     => $this->__('Variation %s', $counter),
                'content'   => $tab->toHtml(),
            ));
        }

        $this->_updateActiveTab();
        return parent::_beforeToHtml();
    }

    protected function _updateActiveTab()
    {
        $tabId = $this->getRequest()->getParam('tab');
        if ($tabId) {
            $tabId = preg_replace("#{$this->getId()}_#", '', $tabId);
            if ($tabId) {
                $this->setActiveTab($tabId);
            }
        }
    }
}
