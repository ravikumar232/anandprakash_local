<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Block_Adminhtml_Test_Edit_Tab_Report extends Mage_Adminhtml_Block_Template
{
    protected function _construct()
    {
        $this->setTemplate('amasty/amabtesting/test/report.phtml');

        parent::_construct();
    }

    public function getTest()
    {
        return Mage::registry('current_test');
    }

    public function canApplyToProduct()
    {
        $test = $this->getTest();

        if ($test->getStatus() != Amasty_Abtesting_Model_Test::STATUS_COMPLETE)
            return false;

        if ($test->getInvolvedProducts()->getSize() == 1)
            return true;
        else
            return false;
    }
}
