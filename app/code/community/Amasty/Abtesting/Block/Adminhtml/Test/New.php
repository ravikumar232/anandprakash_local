<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Block_Adminhtml_Test_New extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_blockGroup = 'amabtesting';
        $this->_controller = 'adminhtml_test';
        $this->_mode = 'new';

        $this->_updateButton('save', 'label', $this->__('Continue'));
    }

    public function getHeaderText()
    {
        $header = $this->__('New test');

        return $header;
    }
}
