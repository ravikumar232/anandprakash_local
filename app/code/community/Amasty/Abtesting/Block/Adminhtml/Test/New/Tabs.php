<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Block_Adminhtml_Test_New_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('testTabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->__('Test Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('general', array(
            'label'     => $this->__('General'),
            'title'     => $this->__('General'),
            'content'   => $this->getLayout()
                ->createBlock('amabtesting/adminhtml_test_new_tab_general')->toHtml(),
        ));

        $grid = $this->getLayout()->createBlock('amabtesting/adminhtml_test_new_tab_products');


        $serializer = $this->getLayout()->createBlock('adminhtml/widget_grid_serializer');
        $serializer->initSerializerBlock($grid, 'getProductIds', 'product_ids', 'product_ids');

        $this->addTab('products', array(
            'label'     => $this->__('Products to test'),
            'title'     => $this->__('Products to test'),
            'content'   => $grid->toHtml() . $serializer->toHtml() . '
                <script>$$("[name=product_ids]")[0].addClassName("required-entry");</script>',
        ));

        $this->addTab('attributes', array(
            'label'     => $this->__('Attributes to test'),
            'title'     => $this->__('Attributes to test'),
            'content'   => $this->getLayout()
                ->createBlock('amabtesting/adminhtml_test_new_tab_attributes')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}