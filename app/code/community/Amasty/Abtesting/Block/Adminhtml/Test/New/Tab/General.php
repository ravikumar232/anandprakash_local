<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Block_Adminhtml_Test_New_Tab_General extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getData('action'),
            'method' => 'post'
        ));

        $fieldset = $form->addFieldset('common_fieldset',
            array('legend' => $this->__('Main Info'))
        );

        $fieldset->addType(
            'amasty_abtesting_number',
            'Amasty_Abtesting_Block_Adminhtml_Form_Element_Number'
        );

        $fieldset->addField('name', 'text', array(
            'name'  => 'name',
            'label' => $this->__('Experiment Name'),
            'title' => $this->__('Experiment Name'),
            'required' => true,
        ));

        $fieldset->addField('variants', 'amasty_abtesting_number', array(
            'name'  => 'variants',
            'label' => $this->__('Number of variants'),
            'title' => $this->__('Number of variants'),
            'min'   => 2,
            'max'   => Amasty_Abtesting_Model_Test::MAX_VARIANTS,
            'required' => true,
            'value' => 2,
            'class' => 'validate-number validate-number-range number-range-2-'.Amasty_Abtesting_Model_Test::MAX_VARIANTS,
        ));

        $this->setForm($form);

        return parent::_prepareForm();
    }
}
