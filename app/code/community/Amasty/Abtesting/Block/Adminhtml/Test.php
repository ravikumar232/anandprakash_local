<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Block_Adminhtml_Test extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_test';
        $this->_blockGroup = 'amabtesting';
        $this->_headerText = $this->__('Tests');
        $this->_addButtonLabel = $this->__('Add Test');

        parent::__construct();
    }
}
