<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


$this->startSetup();

$config = Mage::getConfig()->getNode('default/amabtesting');

foreach ($config as $configEntry) {
    foreach ($configEntry as $sectionName => $section) {
        foreach ($section as $settingName => $setting) {
            Mage::getModel('core/config')->saveConfig(
                "amabtesting/$sectionName/$settingName", (string)$setting
            );
        }
    }
}

Mage::getConfig()->cleanCache();

$this->run("
CREATE TABLE IF NOT EXISTS `{$this->getTable('amabtesting/test')}` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) NOT NULL,
  `created_at` datetime NOT NULL,
  `started_at` DATETIME NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `goal` tinyint(1) unsigned NOT NULL,
  `visit_url` varchar(255) NOT NULL,
  `autocomplete` tinyint(1) UNSIGNED NOT NULL,
  `detect_rate` float UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `{$this->getTable('amabtesting/test_attribute')}` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `test_id` int(10) unsigned NOT NULL,
  `attribute_id` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `test_id` (`test_id`,`attribute_id`),
  CONSTRAINT `{$this->getTable('amabtesting/test_attribute')}_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `{$this->getTable('amabtesting/test')}` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `{$this->getTable('amabtesting/test_product')}` (
  `test_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  KEY `test_id` (`test_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `{$this->getTable('amabtesting/test_product')}_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `{$this->getTable('catalog/product')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `{$this->getTable('amabtesting/test_product')}_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `{$this->getTable('amabtesting/test')}` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `{$this->getTable('amabtesting/test_variation')}` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `test_id` int(10) unsigned NOT NULL,
  `metaproduct_id` INT(10) UNSIGNED NOT NULL,
  `name` VARCHAR(127) NOT NULL,
  `visits` int(10) unsigned NOT NULL,
  `conversions` int(10) unsigned NOT NULL,
  `rate` FLOAT NOT NULL DEFAULT '1.0',
  PRIMARY KEY (`id`),
  KEY `test_id` (`test_id`),
  KEY `metaproduct_id` (`metaproduct_id`),
  CONSTRAINT `{$this->getTable('amabtesting/test_variation')}_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `{$this->getTable('amabtesting/test')}` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `{$this->getTable('amabtesting/test_variation')}_ibfk_2` FOREIGN KEY (`metaproduct_id`) REFERENCES `{$this->getTable('catalog/product')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `{$this->getTable('amabtesting/conversion')}` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `visitor_id` int(10) unsigned NOT NULL,
  `variation_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `visitor_id` (`visitor_id`),
  KEY `variation_id` (`variation_id`),
  CONSTRAINT `{$this->getTable('amabtesting/conversion')}_ibfk_1` FOREIGN KEY (`variation_id`) REFERENCES `{$this->getTable('amabtesting/test_variation')}` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8
");

$this->addAttribute('catalog_product', 'amabtesting_css_override', array(
    'group'         => 'Design',
    'backend'       => '',
    'frontend'      => '',
    'class'         => '',
    'default'       => '',
    'label'         => 'CSS Override',
    'input'         => 'textarea',
    'type'          => 'text',
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'       => 0,
    'required'      => 0,
    'searchable'    => 0,
    'filterable'    => 0,
    'unique'        => 0,
    'comparable'    => 0,
    'visible_on_front' => 0,
    'is_configurable' => 0,
    'is_html_allowed_on_front' => 0,
    'user_defined'  => 1,
));

$this->endSetup();
