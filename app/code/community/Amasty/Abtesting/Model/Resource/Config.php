<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Model_Resource_Config extends Mage_Core_Model_Resource
{
    protected $_configCache = null;

    public function getDbConfig($path, $default = null)
    {
        if ($this->_configCache === null)
        {
            $resource = Mage::getSingleton('core/resource');
            $adapter = $resource->getConnection('core_read');

            $select = $adapter->select()
                ->from($resource->getTableName('core/config_data'), array('path', 'value'))
                ->where('path LIKE \'amabtesting/%\'')
                ->where('scope_id=?', 0)
            ;

            $this->_configCache = $adapter->fetchAll($select, array(), PDO::FETCH_KEY_PAIR);
        }

        if (isset($this->_configCache[$path]))
            return $this->_configCache[$path];
        else
            return $default;
    }
}
