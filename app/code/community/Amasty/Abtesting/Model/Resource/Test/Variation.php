<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Model_Resource_Test_Variation extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('amabtesting/test_variation', 'id');
    }

    /**
     * Fix save() method crash when it called before Magento initialization
     * @param mixed  $value
     * @param string $type
     *
     * @return mixed
     */
    protected function _prepareTableValueForSave($value, $type)
    {
        return $value;
    }
}
