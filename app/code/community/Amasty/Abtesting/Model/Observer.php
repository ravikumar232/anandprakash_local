<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Model_Observer
{
    public function hideMetaProducts($observer)
    {
        $observer->getCollection()->addFieldToFilter(
            'type_id',
            array('neq' => Amasty_Abtesting_Model_Test_Variation::META_PRODUCT_TYPE)
        );
    }

    public function onProductInitAfter($observer)
    {
        $product = $observer->getProduct();

        $test = Mage::getSingleton('amabtesting/test')->findByProductId(
            $product->getId()
        );

        if ($test && $test->getId()) {
            Mage::register('amabtesting_test', $test, true);
        }
        else
            return;

        $visitor = Mage::getSingleton('amabtesting/visitor');

        $variationIndex = +Mage::app()->getRequest()->getParam(
            Amasty_Abtesting_Model_Visitor::VARIATION_GET_PARAM, false
        );

        if (!$variationIndex && $visitor->isIgnored())
            return;

        $variation = $visitor->getCurrentVariation($product->getId(), $variationIndex);

        if (!$variation || !$variation->getId())
            return;

        Mage::register('amabtesting_variation', $variation, true);

        $metaproduct = $variation->getMetaproduct();

        $test = Mage::getModel('amabtesting/test')->load($variation->getTestId());

        foreach ($test->getAttributes() as $attribute) {
            $code = $attribute->getAttributeCode();
            $value = $metaproduct->getData($code);

            $product->setData($code, $value);
        }
    }

    public function onProductLayoutRenderBefore($observer)
    {
        $product = Mage::registry('current_product');

        if ($css = $product->getData('amabtesting_css_override')) {
            $cssBlock = Mage::app()->getLayout()->createBlock(
                'core/text',
                'amabtesting_css_override'
            );

            $cssBlock->setText("<style>$css</style>");

            Mage::app()->getLayout()
                ->getBlock('head')
                ->insert($cssBlock);
        }
    }

    public function checkCompletion($observer)
    {
        $tests = Mage::getResourceModel('amabtesting/test_collection')
            ->addActiveFilter()
        ;

        foreach ($tests as $test) {
            $test->checkCompletion(true);
        }
    }

    public function updateRates($observer)
    {
        $tests = Mage::getResourceModel('amabtesting/test_collection')
            ->addActiveFilter()
        ;

        foreach ($tests as $test) {
            $test->updateRates();
        }
    }
}
