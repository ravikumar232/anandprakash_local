<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Model_Conversion extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('amabtesting/conversion');
    }

    public function createConversion($goal, $product = false)
    {
        $visitor = Mage::getSingleton('amabtesting/visitor');

        $visitorId = $visitor->getVisitorId();
        $involvedTests = $visitor->getInvolvedTests();

        if ($visitor && !empty($involvedTests)) {
            $tests = Mage::getResourceModel('amabtesting/test_collection')
                ->addActiveFilter()
                ->addFieldToFilter('goal', $goal)
                ->addFieldToFilter('id', array('in' => array_keys($involvedTests)))
                ->setOrder('main_table.id', 'ASC')
            ;

            if ($product) {
                $tests->addProductFilter($product);
            }

            if ($goal == Amasty_Abtesting_Model_Test::GOAL_VISIT) {
                $tests->addFieldToFilter(
                    'visit_url',
                    strtok(Mage::app()->getRequest()->getServer('REQUEST_URI'), '?')
                );
            }

            $tests->getSelect()->distinct();

            foreach ($tests as $test) {
                $variation = Mage::getModel('amabtesting/test_variation')->load(
                    $involvedTests[$test->getId()]
                );

                if ($variation->getId()) {

                    $conversion = Mage::getResourceModel('amabtesting/conversion_collection')
                        ->addFieldToFilter('visitor_id', $visitorId)
                        ->addFieldToFilter('variation_id', $variation->getId())
                        ->getFirstItem()
                    ;

                    if (!$conversion->getId()) {
                        $conversion
                            ->setVisitorId($visitorId)
                            ->setVariationId($variation->getId())
                            ->save()
                        ;

                        $variation->setConversions($variation->getConversions()+1);
                        $variation->save();
                    }
                }
            }
        }
    }
}
