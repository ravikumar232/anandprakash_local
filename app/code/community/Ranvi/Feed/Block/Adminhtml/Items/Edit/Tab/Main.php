<?php

class Ranvi_Feed_Block_Adminhtml_Items_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form

{

	

	

    protected function _prepareForm()

    {

        

        $form = new Varien_Data_Form();

        

        if(Mage::registry('ranvi_feed')){

        	$item = Mage::registry('ranvi_feed');

        }else{

        	$item = new Varien_Object();

        }

        

        $this->setForm($form);

        $fieldset = $form->addFieldset('main_fieldset', array('legend' => $this->__('Item information')));


     	$fieldset->addField('type', 'hidden', array(

            'name'      => 'type',

        ));

     	

    	$fieldset->addField('name', 'text', array(

            'name'      => 'name',

            'label'     => $this->__('Name'),

            'title'     => $this->__('Name'),

            'required'  => true,

        ));

        if($item->getId() && ($url = $item->getUrl())){

        	

		        $fieldset->addField('comments', 'note', array(

		            'label'    => $this->__('Access Url'),

		            'title'    => $this->__('Access Url'),

		            'text'    => '<a href="'.$url.'" target="_blank">'.$url.'</a>',

		        ));	

        }

        

        $fieldset->addField('filename', 'text', array(

            'name'      => 'filename',

            'label'     => $this->__('Filename'),

            'title'     => $this->__('Filename'),

            'required'  => false,

        ));

        

        $fieldset->addField('store_id', 'select', array(

            'label'     => $this->__('Store View'),

            'required'  => true,

            'name'      => 'store_id',

            'values'    => Mage::getModel('ranvi_feed/adminhtml_system_config_source_store')->getStoreValuesForForm(),

        ));
		
		/*
		$fieldset2 = $form->addFieldset('main_fieldset2', array('legend' => $this->__('Inclusions/Exclusions')));
		
		//$ind_url=(empty($item->getIncludedSkus()))?"":"<a href='".$this->getBaseUrl().$item->getIncludedSkus()."'>download old file</a>";
		//$exc_url=(empty($item->getExcludedSkus()))?"":"<a href='".$this->getBaseUrl().$item->getExcludedSkus()."'>download old file</a>";
		
		
		$fieldset2->addField('file1', 'file', array(
          'label'     => $this->__('List of skus to be included'),
          'name'  => 'included_skus',
          'after_element_html' => '<br><small>One column csv file containing skus</small>',
		  'note'=>$ind_url,
          'tabindex' => 1
        ));
		
		
		$fieldset2->addField('file2', 'file', array(
          'label'     => $this->__('List of skus to be excluded'),
          'name'  => 'excluded_skus',
          'after_element_html' => '<br><small>One column csv file containing skus</small>',
		  'note'=>$exc_url,
          'tabindex' => 1
        ));
		*/

        if(!$item->getType() && $this->getRequest()->getParam('type')){

        	$item->setType($this->getRequest()->getParam('type'));

        }

        

        $form->setValues($item->getData());

        

        

        return parent::_prepareForm();

        

    }

    

  

}
