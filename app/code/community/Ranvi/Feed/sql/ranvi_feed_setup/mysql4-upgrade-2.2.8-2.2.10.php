<?php

$installer = $this;

$installer->startSetup();

$installer->run("
ALTER TABLE `{$this->getTable('ranvi_feed_entity')}`
  ADD COLUMN `excluded_skus` varchar(100) ;
");
$installer->run("
ALTER TABLE `{$this->getTable('ranvi_feed_entity')}`
  ADD COLUMN `included_skus` varchar(100) ;
");

$installer->endSetup();