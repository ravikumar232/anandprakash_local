<?php
class Mage_Adminhtml_Model_System_Config_Source_Diyoptions15166090683
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
		
            array('value' => 1, 'label'=>Mage::helper('adminhtml')->__('left')),
            array('value' => 2, 'label'=>Mage::helper('adminhtml')->__('right')),
            array('value' => 3, 'label'=>Mage::helper('adminhtml')->__('center')),
        );
    }

}
