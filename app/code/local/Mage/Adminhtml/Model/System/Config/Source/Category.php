<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright  Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Config category source
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Adminhtml_Model_System_Config_Source_Category
{
     function getCategoriesTreeView() {
        // Get category collection
        $categories = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('name')
            ->addAttributeToSort('path', 'asc')
            ->addFieldToFilter('is_active', array('eq'=>'1'))
            ->load()
            ->toArray();
    
        // Arrange categories in required array
        $categoryList = array();
        foreach ($categories as $catId => $category) {
            if (isset($category['name'])) {
                $categoryList[] = array(
                    'label' => $category['name'],
                    'level'  =>$category['level'],
                    'value' => $catId
                );
            }
        }
        return $categoryList;
    }
    
    
    // Return options to system config
    
 
    public function toOptionArray()
    {
        
        $options = array();
 
        $options[] = array(
            'label' => Mage::helper('megamenu')->__('-- None --'),
            'value' => ''
        );
        
        
        $categoriesTreeView = $this->getCategoriesTreeView();
 
        foreach($categoriesTreeView as $value)
        {
            $catName    = $value['label'];
            $catId      = $value['value'];
            $catLevel    = $value['level'];
            
            $hyphen = '-';
            for($i=1; $i<$catLevel; $i++){
                $hyphen = $hyphen ."-";
            }
            
            $catName = $hyphen .$catName;
            
            $options[] = array(
               'label' => $catName,
               'value' => $catId
            );
            
            
        }
        
        return $options;
        
    }
}
