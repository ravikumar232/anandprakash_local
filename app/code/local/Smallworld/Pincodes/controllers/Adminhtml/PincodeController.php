<?php

class Smallworld_Pincodes_Adminhtml_PincodeController extends Mage_Adminhtml_Controller_Action
{
		protected function _isAllowed()
		{
		//return Mage::getSingleton('admin/session')->isAllowed('pincodes/pincode');
			return true;
		}

		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("pincodes/pincode")->_addBreadcrumb(Mage::helper("adminhtml")->__("Pincode  Manager"),Mage::helper("adminhtml")->__("Pincode Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("Pincodes"));
			    $this->_title($this->__("Manager Pincode"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{			    
			    $this->_title($this->__("Pincodes"));
				$this->_title($this->__("Pincode"));
			    $this->_title($this->__("Edit Item"));
				
				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("pincodes/pincode")->load($id);
				if ($model->getId()) {
					Mage::register("pincode_data", $model);
					$this->loadLayout();
					$this->_setActiveMenu("pincodes/pincode");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Pincode Manager"), Mage::helper("adminhtml")->__("Pincode Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Pincode Description"), Mage::helper("adminhtml")->__("Pincode Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("pincodes/adminhtml_pincode_edit"))->_addLeft($this->getLayout()->createBlock("pincodes/adminhtml_pincode_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("pincodes")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

		$this->_title($this->__("Pincodes"));
		$this->_title($this->__("Pincode"));
		$this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("pincodes/pincode")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("pincode_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("pincodes/pincode");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Pincode Manager"), Mage::helper("adminhtml")->__("Pincode Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Pincode Description"), Mage::helper("adminhtml")->__("Pincode Description"));


		$this->_addContent($this->getLayout()->createBlock("pincodes/adminhtml_pincode_edit"))->_addLeft($this->getLayout()->createBlock("pincodes/adminhtml_pincode_edit_tabs"));

		$this->renderLayout();

		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {

					try {

						

						$model = Mage::getModel("pincodes/pincode")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Pincode was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setPincodeData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $model->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setPincodeData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$model = Mage::getModel("pincodes/pincode");
						$model->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}

		
		public function massRemoveAction()
		{
			try {
				$ids = $this->getRequest()->getPost('ids', array());
				foreach ($ids as $id) {
                      $model = Mage::getModel("pincodes/pincode");
					  $model->setId($id)->delete();
				}
				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
			}
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
			}
			$this->_redirect('*/*/');
		}
			
		/**
		 * Export order grid to CSV format
		 */
		public function exportCsvAction()
		{
			$fileName   = 'pincode.csv';
			$grid       = $this->getLayout()->createBlock('pincodes/adminhtml_pincode_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
		} 
		/**
		 *  Export order grid to Excel XML format
		 */
		public function exportExcelAction()
		{
			$fileName   = 'pincode.xml';
			$grid       = $this->getLayout()->createBlock('pincodes/adminhtml_pincode_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
		}
}
