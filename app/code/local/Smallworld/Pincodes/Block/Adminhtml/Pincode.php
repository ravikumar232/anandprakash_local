<?php


class Smallworld_Pincodes_Block_Adminhtml_Pincode extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_pincode";
	$this->_blockGroup = "pincodes";
	$this->_headerText = Mage::helper("pincodes")->__("Pincode Manager");
	$this->_addButtonLabel = Mage::helper("pincodes")->__("Add New Item");
	parent::__construct();
	
	}

}