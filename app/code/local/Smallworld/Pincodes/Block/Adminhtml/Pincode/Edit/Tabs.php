<?php
class Smallworld_Pincodes_Block_Adminhtml_Pincode_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("pincode_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("pincodes")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("pincodes")->__("Item Information"),
				"title" => Mage::helper("pincodes")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("pincodes/adminhtml_pincode_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
