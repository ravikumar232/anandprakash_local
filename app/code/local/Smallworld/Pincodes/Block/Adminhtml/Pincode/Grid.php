<?php

class Smallworld_Pincodes_Block_Adminhtml_Pincode_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("pincodeGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("pincodes/pincode")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("pincodes")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
				));
                
				$this->addColumn("pincode", array(
				"header" => Mage::helper("pincodes")->__("Pincode"),
				"index" => "pincode",
				));
				$this->addColumn("prepaid_enabled", array(
				"header" => Mage::helper("pincodes")->__("Enable Prepaid"),
				"index" => "prepaid_enabled",
				));
				$this->addColumn("cod_enabled", array(
				"header" => Mage::helper("pincodes")->__("Enable COD"),
				"index" => "cod_enabled",
				));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_pincode', array(
					 'label'=> Mage::helper('pincodes')->__('Remove Pincode'),
					 'url'  => $this->getUrl('*/adminhtml_pincode/massRemove'),
					 'confirm' => Mage::helper('pincodes')->__('Are you sure?')
				));
			return $this;
		}
			

}