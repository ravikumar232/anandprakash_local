<?php
class Smallworld_Pincodes_Block_Adminhtml_Pincode_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("pincodes_form", array("legend"=>Mage::helper("pincodes")->__("Item information")));

				
						$fieldset->addField("pincode", "text", array(
						"label" => Mage::helper("pincodes")->__("Pincode"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "pincode",
						));
					
						$fieldset->addField("prepaid_enabled", "text", array(
						"label" => Mage::helper("pincodes")->__("Enable Prepaid"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "prepaid_enabled",
						));
					
						$fieldset->addField("cod_enabled", "text", array(
						"label" => Mage::helper("pincodes")->__("Enable COD"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "cod_enabled",
						));
					

				if (Mage::getSingleton("adminhtml/session")->getPincodeData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getPincodeData());
					Mage::getSingleton("adminhtml/session")->setPincodeData(null);
				} 
				elseif(Mage::registry("pincode_data")) {
				    $form->setValues(Mage::registry("pincode_data")->getData());
				}
				return parent::_prepareForm();
		}
}
