<?php
class Smallworld_Pincodes_Model_Mysql4_Pincode extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("pincodes/pincode", "id");
    }
}