<?php
class Smallworld_Megamenu_Block_Adminhtml_Megamenu_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("megamenu_form", array("legend"=>Mage::helper("megamenu")->__("Item information")));

								
						$value_select1 = $fieldset->addField('enable_disable', 'select', array(
						'label'     => Mage::helper('megamenu')->__('Enable-Disable'),
						'values'   => Smallworld_Megamenu_Block_Adminhtml_Megamenu_Grid::getValueArray0(),
						'name' => 'enable_disable',
						));				
						$cat= $fieldset->addField('category', 'select', array(
						'label'     => Mage::helper('megamenu')->__('Category'),
						'values'   => Smallworld_Megamenu_Block_Adminhtml_Megamenu_Grid::getValueArray1(),
						'name' => 'category',
						));				
						$img = $fieldset->addField('image', 'image', array(
						'label' => Mage::helper('megamenu')->__('Thumbnail'),
						'name' => 'image',
						'note' => '(*.jpg, *.png, *.gif)',
						));				
						
						$level = $fieldset->addField("columnlevel", "text", array(
						"label" => Mage::helper("megamenu")->__("Columnlevel"),
						"name" => "columnlevel",
						));
						$value_select = $fieldset->addField('custommenuenable', 'select', array(
						'label'     => Mage::helper('megamenu')->__('Custom Menu Enable-Disable'),
						'values'   => Smallworld_Megamenu_Block_Adminhtml_Megamenu_Grid::getValueArray3(),
						'name' => 'custommenuenable',
						));
						$custm = $fieldset->addField("custommenu", "text", array(
						"label" => Mage::helper("megamenu")->__("Custom Menu"),
						"name" => "custommenu",
						
						));
						$custk = $fieldset->addField("menuurlkey", "text", array(
						"label" => Mage::helper("megamenu")->__("Menu Url Key"),
						"name" => "menuurlkey",
						
						));
						$this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
						->addFieldMap($value_select->getHtmlId(), $value_select->getName())
						->addFieldMap($value_select1->getHtmlId(), $value_select1->getName())
					   ->addFieldMap($custm->getHtmlId(), $custm->getName())
					   ->addFieldMap($custk->getHtmlId(), $custk->getName())
					   ->addFieldMap($cat->getHtmlId(), $cat->getName())
					   ->addFieldMap($img->getHtmlId(), $img->getName())
					   ->addFieldMap($level->getHtmlId(), $level->getName())
					   ->addFieldDependence(
						   $custm->getName(),
						 
						   $value_select->getName(),
						   '0'
					   )
					   ->addFieldDependence(
						   $custm->getName(),
						   $value_select->getName(),
						   '1'
					   )
					   ->addFieldDependence(
						   $custk->getName(),
						 
						   $value_select->getName(),
						   '0'
					   )
					   ->addFieldDependence(
						   $custk->getName(),
						   $value_select->getName(),
						   '1'
					   )
					   ->addFieldDependence(
						   $cat->getName(),
						 
						   $value_select1->getName(),
						   '0'
					   )
					   ->addFieldDependence(
						   $cat->getName(),
						   $value_select1->getName(),
						   '1'
					   )
					   ->addFieldDependence(
						   $img->getName(),
						 
						   $value_select1->getName(),
						   '0'
					   )
					   ->addFieldDependence(
						   $img->getName(),
						   $value_select1->getName(),
						   '1'
					   )
					   ->addFieldDependence(
						   $level->getName(),
						 
						   $value_select1->getName(),
						   '0'
					   )
					   ->addFieldDependence(
						   $level->getName(),
						   $value_select1->getName(),
						   '1'
					   )

				   );
		

				if (Mage::getSingleton("adminhtml/session")->getMegamenuData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getMegamenuData());
					Mage::getSingleton("adminhtml/session")->setMegamenuData(null);
				} 
				elseif(Mage::registry("megamenu_data")) {
				    $form->setValues(Mage::registry("megamenu_data")->getData());
				}
				return parent::_prepareForm();
		}
}
