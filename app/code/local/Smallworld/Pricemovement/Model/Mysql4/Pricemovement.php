<?php
class Smallworld_Pricemovement_Model_Mysql4_Pricemovement extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("pricemovement/pricemovement", "id");
    }
     protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        if (!$object->getCreatedAt()) {
            $object->setCreatedAt($this->formatDate(time()));
        }

        return parent::_beforeSave($object);
    }

    public function insertStocksMovements($s)
    {
        $this->_getWriteAdapter()->insertMultiple($this->getMainTable(), $s);

        return $this;
    }
}