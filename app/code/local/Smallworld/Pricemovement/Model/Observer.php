<?php
class Smallworld_Pricemovement_Model_Observer
{
    public function addPriceMovementsTab()
    {
        $layout = Mage::getSingleton('core/layout');
        $block = $layout->getBlock('product_tabs');
        if ($block && $block->getProduct() && $block->getProduct()->getTypeId() == 'simple') {
            $block->addTab('pricemovement', array(
                'after' => 'prices',
                'label' => Mage::helper('pricemovement')->__('Price Movements'),
                'content' => $layout->createBlock('pricemovement/adminhtml_pricemovement_grid')->toHtml(),
            ));
        }
    }
		    public function catalogproductsavebefore(Varien_Event_Observer $observer)
			 {
				$products=$observer->getProduct();
          $p2=$products->getPrice();
       Mage::getSingleton('adminhtml/session')->setSessionVariable($p2);
        return $this;
         }
		  public function catalogproductsaveafter(Varien_Event_Observer $observer)
			{  
          $product=$observer->getProduct();
         $beforeP = Mage::getSingleton('adminhtml/session')->getSessionVariable();
         if($beforeP!=$product->getPrice())
         {
           $this->insertpriceMovement($product,$beforeP);
           return $this;
         }
        }
			public function insertpriceMovement($p,$b)
        {
         $dateTime = date_create('now')->format('Y-m-d H:i:s');
            Mage::getModel('pricemovement/pricemovement')
            	 ->setSku($p->getSku())
            	  ->setBeforeprice($b)
            	 ->setAfterprice($p->getPrice())
                ->setUsername($this->_getUsername())
                 ->setMessage('saved manually')
                ->setDate($dateTime)
                ->save();
            Mage::getModel('catalog/product')->load($p->getProductId())->cleanCache();
        
          }
   
	   protected function _getUsername()
        {
        $username = '-';
        if (Mage::getSingleton('api/session')->isLoggedIn()) {
            $username = Mage::getSingleton('api/session')->getUser()->getUsername();
        } elseif (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $username = Mage::getSingleton('customer/session')->getCustomer()->getName();
        } elseif (Mage::getSingleton('admin/session')->isLoggedIn()) {
            $username = Mage::getSingleton('admin/session')->getUser()->getUsername();
        }
		
        return $username;
}

		
}
