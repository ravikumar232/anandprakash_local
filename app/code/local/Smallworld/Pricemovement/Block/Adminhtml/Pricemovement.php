<?php


class Smallworld_Pricemovement_Block_Adminhtml_Pricemovement extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{
    parent::__construct();
	$this->_controller = "adminhtml_pricemovement";
	$this->_blockGroup = "pricemovement";
	$this->_headerText = Mage::helper("pricemovement")->__("Price Movements");
	 $this->_removeButton('add');
	
	
	}
	 protected function _prepareLayout()
    {
        $this->setChild('grid', $this->getLayout()->createBlock(
            'pricemovement/adminhtml_pricemovement_grid',
            'pricemovement.grid'
        ));

        return parent::_prepareLayout();
    }

    public function getHeaderCssClass()
    {
        return '';
    }

}