<?php

class Smallworld_Pricemovement_Block_Adminhtml_Pricemovement_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("pricemovementGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("ASC");
				$this->setSaveParametersInSession(true);
		}
		 public function getProduct()
   		 {
       	 return Mage::registry('current_product');
   		 }

		protected function _prepareCollection()
		{   
			    if($this->getProduct())
			    {
				$collection = Mage::getModel("pricemovement/pricemovement")->getCollection();
				$collection->addFieldToFilter('sku', $this->getProduct()->getSku());
				$this->setCollection($collection);
				return parent::_prepareCollection();
			}
			else
			{
				$collection = Mage::getModel("pricemovement/pricemovement")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
			}
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("pricemovement")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
				));
				$this->addColumn("sku", array(
				"header" => Mage::helper("pricemovement")->__("Sku"),
				"index" => "sku",
				));
                
				$this->addColumn("beforeprice", array(
				"header" => Mage::helper("pricemovement")->__("Before save Price"),
				"index" => "beforeprice",
				));
				$this->addColumn("afterprice", array(
				"header" => Mage::helper("pricemovement")->__("After save Price"),
				"index" => "afterprice",
				));
				$this->addColumn("message", array(
				"header" => Mage::helper("pricemovement")->__("Message"),
				"index" => "message",
				));
				$this->addColumn("username", array(
				"header" => Mage::helper("pricemovement")->__("User"),
				"index" => "username",
				));
					$this->addColumn('date', array(
						'header'    => Mage::helper('pricemovement')->__('Date'),
						'index'     => 'date',
						'type'      => 'timestamp',
					));

				return parent::_prepareColumns();
		}

		


		
	
			

}