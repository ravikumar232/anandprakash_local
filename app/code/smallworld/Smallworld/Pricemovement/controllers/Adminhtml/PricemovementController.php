<?php

class Smallworld_Pricemovement_Adminhtml_PricemovementController extends Mage_Adminhtml_Controller_Action
{
		protected function _isAllowed()
		{
			return true;
		}

		protected function _initAction()
		{
		$this->_title($this->__('Swcustommenu'))
            ->_title(Mage::helper('pricemovement')->__('Price Movements'));
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('pricemovement/adminhtml_pricemovement'));
        $this->_setActiveMenu('swcustommenu/pricemovement');

       
		}
		public function indexAction() 
		{

				$this->_initAction();
				$this->renderLayout();
		}
		
			
}
