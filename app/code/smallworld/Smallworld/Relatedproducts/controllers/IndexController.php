<?php
class Smallworld_Relatedproducts_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
      
	  $this->loadLayout();   
	  $this->getLayout()->getBlock("head")->setTitle($this->__("Titlename"));
	        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
		   ));

      $breadcrumbs->addCrumb("titlename", array(
                "label" => $this->__("Titlename"),
                "title" => $this->__("Titlename")
		   ));

      $this->renderLayout(); 
	  
    }
		public function ajaxAction()
		{
		    $getproductno=Mage::getStoreConfig('related_products/modules_configuration/no_product');
		    $getwidth=Mage::getStoreConfig('related_products/modules_configuration/width_image');
		    $getheight=Mage::getStoreConfig('related_products/modules_configuration/height_image');
		    $id = $this->getRequest()->getParam('categoryId');
		    $category = Mage::getModel('catalog/category')->load($id);
		    $products = $category->getProductCollection()->addCategoryFilter($category)->addAttributeToSelect('*');
		    $products->getSelect()->order(new Zend_Db_Expr('RAND()'));
		    $products->getSelect()->limit($getproductno);		
		    $res = array();
		    if(!empty($products))
		    {
		       foreach($products as $product)
		         {
		           $_finalPrice = Mage::helper('core')->currency($product->getFinalPrice(),true,false);
		           $total['data'] = ["name" => $product->getName(), "image" => $product->getImageUrl(),"imgwidth" =>$getwidth,"imgheight" =>$getheight ,"price" =>$_finalPrice,"producturl" =>$product->getProductUrl()];
		          array_push($res,$total['data']);
		         }
		    }
		$reslt = json_encode($res);
		echo $reslt;
	 }
		

		public function newarrivalsAction() 
		{
		     $getproductno=Mage::getStoreConfig('related_products/newarrival_configuration/no_product_newarrival');
		     $getwidth=Mage::getStoreConfig('related_products/newarrival_configuration/width_image_newarrival');
		     $getheight=Mage::getStoreConfig('related_products/newarrival_configuration/height_image_newarrival');
		     $id = $this->getRequest()->getParam('categoryId');
		     $category = Mage::getModel('catalog/category')->load($id);
		     $products = $category->getProductCollection()->addCategoryFilter($category) ->addAttributeToSelect('*');
		     $products->getSelect()->order(new Zend_Db_Expr('RAND()'));
		     $products->getSelect()->limit($getproductno);
		     $res = array();
		       if(!empty($products))
			     {
		          foreach($products as $product)
		           {
		             $_finalPrice = Mage::helper('core')->currency($product->getFinalPrice(),true,false);
		             $total['data'] = ["name" => $product->getName(), "image" => $product->getImageUrl(),"imgwidth" =>
		             $getwidth,"imgheight" =>$getheight ,"price" =>$_finalPrice,"producturl" =>$product->getProductUrl()];
		             array_push($res,$total['data']);
		            }
		        }
		             $reslt = json_encode($res);
		
		            echo $reslt;
		}
	
  }