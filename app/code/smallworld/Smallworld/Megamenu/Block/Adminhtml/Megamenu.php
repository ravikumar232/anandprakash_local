<?php


class Smallworld_Megamenu_Block_Adminhtml_Megamenu extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_megamenu";
	$this->_blockGroup = "megamenu";
	$this->_headerText = Mage::helper("megamenu")->__("Megamenu Manager");
	$this->_addButtonLabel = Mage::helper("megamenu")->__("Add New Item");
	parent::__construct();
	
	}

}