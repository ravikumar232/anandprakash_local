<?php

class Smallworld_Megamenu_Block_Adminhtml_Megamenu_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("megamenuGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("megamenu/megamenu")->getCollection();
				$this->setCollection($collection);
				
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("megamenu")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
				));
                
						$this->addColumn('enable_disable', array(
						'header' => Mage::helper('megamenu')->__('Enable-Disable'),
						'index' => 'enable_disable',
						'type' => 'options',
						'options'=>Smallworld_Megamenu_Block_Adminhtml_Megamenu_Grid::getOptionArray0(),				
						));
						
						$this->addColumn('category', array(
						'header' => Mage::helper('megamenu')->__('Category'),
						'index' => 'category',
						'type' => 'options',
						'options'=>Smallworld_Megamenu_Block_Adminhtml_Megamenu_Grid::getOptionArray1(),
										
						));
						$this->addColumn("image", array(
						"header" => Mage::helper("megamenu")->__("Image"),
						"index" => "image",
						));
						$this->addColumn("columnlevel", array(
						"header" => Mage::helper("megamenu")->__("Columnlevel"),
						"index" => "columnlevel",
						));
						$this->addColumn('custommenuenable', array(
						'header' => Mage::helper('megamenu')->__('Custom Menu Enable-Disable'),
						'index' => 'custommenuenable',
						'type' => 'options',
						'options'=>Smallworld_Megamenu_Block_Adminhtml_Megamenu_Grid::getOptionArray3(),				
						));
						$this->addColumn("custommenu", array(
						"header" => Mage::helper("megamenu")->__("Custom Menu"),
						"index" => "custommenu",
						));
						$this->addColumn("menuurlkey", array(
						"header" => Mage::helper("megamenu")->__("Menu Url Key"),
						"index" => "menuurlkey",
						));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}
		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_megamenu', array(
					 'label'=> Mage::helper('megamenu')->__('Remove Megamenu'),
					 'url'  => $this->getUrl('*/adminhtml_megamenu/massRemove'),
					 'confirm' => Mage::helper('megamenu')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray0()
		{
            $data_array=array(); 
			$data_array[0]='No';
			$data_array[1]='Yes';
            return($data_array);
		}
		static public function getValueArray0()
		{
            $data_array=array();
			foreach(Smallworld_Megamenu_Block_Adminhtml_Megamenu_Grid::getOptionArray0() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		
		static public function getOptionArray1()
		{
            $data_array=array(); 
			foreach(Mage::getModel('adminhtml/system_config_source_category')->toOptionArray() as $_category){
			 $data_array[$_category['value']] = $_category['label'];
		}
            return($data_array);
         }
		static public function getValueArray1()
		{
      		$data_array=array();
			foreach(Smallworld_Megamenu_Block_Adminhtml_Megamenu_Grid::getOptionArray1() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
			
            return($data_array);

		}
		
		static public function getOptionArray3()
		{
            $data_array=array(); 
			$data_array[0]='No';
			$data_array[1]='Yes';
            return($data_array);
		}
		static public function getValueArray3()
		{
            $data_array=array();
			foreach(Smallworld_Megamenu_Block_Adminhtml_Megamenu_Grid::getOptionArray3() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		

}