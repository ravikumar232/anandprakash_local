(function($) {
    $(document).ready(function() {
        $(window).load(function() {
            setTimeout(function(e) {
                var cart_location_top = $(".add-to-cart-wrapper").offset().top;
                var cart_location_height = $(".add-to-cart-wrapper").height();
                var total_height_inner = parseInt(cart_location_top) + parseInt(cart_location_height);
                var total_height = parseInt(total_height_inner) - ($(window).height());
                if ($(window).height() > total_height) {
                    $(".add-to-cart-wrapper").removeClass("fixed_cart");
                } else {
                    $(".add-to-cart-wrapper").addClass("fixed_cart");
                }
            }, 300)
        });
        setTimeout(function(e) {
            if($(".main-container .product-view .product-essential .product-shop").children(".add-to-cart-wrapper"))
            {
                var cart_location_top = $(".add-to-cart-wrapper").offset().top;
                var cart_location_height = $(".add-to-cart-wrapper").height();
                var total_height_inner = parseInt(cart_location_top) + parseInt(cart_location_height);
                var total_height = parseInt(total_height_inner) - ($(window).height());
                $(window).scroll(function(e) {
                    if ($(window).scrollTop() > total_height) {
                        $(".add-to-cart-wrapper").removeClass("fixed_cart");
                    } else {
                        $(".add-to-cart-wrapper").addClass("fixed_cart");
                    }
                })
            }
        }, 300);
    })

})(jQuery);